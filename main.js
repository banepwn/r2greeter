(function() {
	"use strict";

	const username = document.getElementById("username");
	const password = document.getElementById("password");
	const sessions = document.getElementById("desktop");
	const form = document.querySelector("form");
	const modal = document.getElementById("modal");
	const blackout = document.getElementById("blackout");
	const powerctrl = document.getElementById("acpi");
	const powerdrop = document.getElementById("acpi-dropdown");

	let idle_timer = 0;
	let dead = false;
	function reset_idle() {
		if (dead) return;
		idle_timer = 0;
		blackout.style.transition = "opacity 0.25s";
		blackout.hidden = true;
	};
	["mousedown", "mousemove", "keydown", "scroll", "touchstart"].forEach(event_name => {
		document.addEventListener(event_name, reset_idle, true);
	});
	setInterval(() => {
		if (dead) return;
		if (blackout.hidden && idle_timer > 60) {
			blackout.style.transition = "opacity 5s";
			blackout.hidden = null;
		} else {
			idle_timer++;
		};
	}, 1000);

	let session_index = {};
	lightdm.sessions.forEach((session, index) => {
		session_index[session.key] = index;
		sessions.options[index] = new Option(session.name, index);
	});

	lightdm.users.forEach(user => {
		if (user.logged_in) {
			username.value = user.name;
			password.focus();
			sessions.value = session_index[user.session];
		};
	});

	function show_prompt(title, text, callback) {
		while (modal.firstChild) modal.lastChild.remove();
		const h1 = document.createElement("h1");
		h1.textContent = title ? title : "Message";
		modal.append(h1);
		const p = document.createElement("p");
		p.textContent = text;
		modal.append(p);
		const input = document.createElement("input");
		input.type = "text";
		if (callback) modal.append(input);
		const button = document.createElement("button");
		button.textContent = callback ? "Submit" : "Okay";
		button.addEventListener("click", event => {
			modal.hidden = true;
			if (callback) callback(input.value);
		});
		button.classList.add("spacer");
		modal.append(button);
		modal.hidden = null;
	};

	powerctrl.addEventListener("click", event => {
		if (powerdrop.hidden) {
			powerdrop.hidden = null;
			powerdrop.focus();
		} else {
			powerdrop.hidden = true;
		};
	});
	powerdrop.addEventListener("blur", event => {
		powerdrop.hidden = (event.relatedTarget && event.relatedTarget.closest("#acpi-dropdown")) ? null : true;
	});
	powerdrop.querySelectorAll("li").forEach(el => {
		switch (el.textContent) {
			case "Hibernate":
				el.addEventListener("click", event => { lightdm.hibernate(); });
				break;
			case "Suspend":
				el.addEventListener("click", event => { lightdm.suspend(); });
				break;
			case "Reboot":
			case "Restart":
				el.addEventListener("click", event => { lightdm.restart(); });
				break;
			case "Shut down":
			case "Shutdown":
				el.addEventListener("click", event => { lightdm.shutdown(); });
				break;
		};
		el.addEventListener("blur", event => {
			powerdrop.hidden = (event.relatedTarget && event.relatedTarget.closest("#acpi-dropdown")) ? null : true;
		});
	});

	function form_set_enabled(bool) {
		bool = !bool;
		form.querySelectorAll("input").forEach(el => { el.disabled = bool; });
		form.querySelectorAll("select").forEach(el => { el.disabled = bool; });
	};
	form.addEventListener("submit", event => {
		event.preventDefault();
		if (!username.value) return;
		form_set_enabled(false);
		lightdm.cancel_timed_login();
		lightdm.start_authentication(username.value);
	});

	function die() {
		dead = true;
		blackout.style.transition = "opacity 0.1s";
		blackout.hidden = null;
		setTimeout(() => {
			dead = false;
			form_set_enabled(true);
		}, 3000);
	};

	window.show_prompt = function(message) {
		switch (message) {
			case "Password: ":
				console.log("Providing password");
				lightdm.provide_secret(password.value);
				break;
			default:
				console.warn(`Unexpected prompt: ${message}`);
				show_prompt("Prompt", message, lightdm.provide_secret);
				break;
		};
	};
	window.authentication_complete = function() {
		if (lightdm.is_authenticated) {
			let session = lightdm.sessions[sessions.selectedOptions[0].value || 0];
			console.log(`Successfully authenticated with session ${session.name}`);
			lightdm.login(lightdm.authentication_user, session.key);
			die();
		} else {
			console.error("Failed to authenticate");
			form_set_enabled(true);
			show_prompt("Error", "Incorrect username or password.");
		};
	};

	let music_enabled = false;
	let music;
	document.addEventListener("keydown", event => {
		if (event.shiftKey || event.metaKey || event.altKey) return;
		switch (event.key) {
			case "r":
				location.reload();
				break;
			case "m":
				music_enabled = !music_enabled;
				if (!music) {
					music = new Audio("ascension.mp3");
					music.volume = 0.25;
					music.addEventListener("ended", event => {
						music.currentTime = 0;
						music.play();
					}, false);
					music.play();
				};
				music.muted = !music_enabled;
				break;
		};
	});

	document.body.hidden = null;
})();

