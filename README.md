# r2greeter

![Screenshot](screenshot.png)

> We have developed a new Mind Tech for when you must use your desktop computer. You are strongly encouraged to abstain from using electronics as they are under control by the Threat. However, as you go about your normal life, there may be times when you have no other choice. That is why we have developed this Mind Tech.

This is a theme for [Web Greeter/lightdm-webkit2-greeter](https://github.com/Antergos/web-greeter).

I am in no way affiliated with or supported by Wolfire Games. I am merely a fan paying homage to [Receiver 2](https://www.receiver2.com/), one of their games.

## Installation

See my instructions in the README for my other web-greeter theme, [GlitchDM Fauux](https://bitbucket.org/banepwn/glitchdm-fauux/src/master/README.md).

## License

`mock.js` and `ascension.mp3` are not included in the repository due to license incompatibility. You can get `mock.js` from [Pastebin](https://pastebin.com/J8r7JJP7) (GPLv3), and you can get `ascension.mp3` from [Bandcamp](https://bandcamp.antonriehl.com/track/ascension).

`mock.js` is only neccessary if you want to test it out in your browser. It only does anything if run outside the context of Web Greeter.

`ascension.mp3` is only neccessary if you want to be able to press Ctrl+M to hear the ascension music.

Note that `style.css` tries to very closely replicate the UI of a proprietary game. One might consider it to be non-free for this reason. Distribute it at your own risk.

**The following terms only apply to `README.md`, `index.html`, `main.js`, `style.css`, and the contents of `icons/`.**

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
